package helpers;

import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;

public class Helpers {

    public void sleep(WebDriver driver, int seconds) {
        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }
}

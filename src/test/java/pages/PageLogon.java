package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class PageLogon {
    private WebDriver driver;
    private By titleTextField;

    public PageLogon(WebDriver driver) {
        this.driver = driver;
        titleTextField = By.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/p");

    }

    public void assertLogonPage() {
        String titleTextFieldhExValue = "Welcome back to Mercury Tours! Enter your user information to access the member-only areas of this site. If you don't have a log-in, please fill out the registration form.";

        WebElement titleTextElem = driver.findElement(titleTextField);

        Assert.assertEquals(titleTextElem.getText(), titleTextFieldhExValue, "Page not match");
    }
}

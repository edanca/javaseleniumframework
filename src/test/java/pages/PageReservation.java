package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PageReservation {
    private WebDriver driver;
    private By titleTextField;
    private By passengerComboboxField;
    private By departingFromField;
    private By arrivingInField;

    public PageReservation(WebDriver driver) {
        this.driver = driver;
        titleTextField = By.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td");
        passengerComboboxField = By.name("passCount");
        departingFromField = By.name("fromPort");
        arrivingInField = By.name("toPort");
    }

    public void assertPage() {
        String titleTextFieldExValue = "Use our Flight Finder to search for the lowest fare on participating airlines. Once you've booked your flight, don't forget to visit the Mercury Tours Hotel Finder to reserve lodging in your destination city.";

        WebElement registerParagraphElem = driver.findElement(titleTextField);

        Assert.assertEquals(registerParagraphElem.getText(), titleTextFieldExValue, "Page match");
    }

    public void selectFlightPassengers(int cant) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement passengesQuantity = wait.until(ExpectedConditions.presenceOfElementLocated(passengerComboboxField));
        Select selectPassengerElem = new Select(passengesQuantity);
        selectPassengerElem.selectByVisibleText(Integer.toString(cant));
    }

    public void selectDepartingFrom(int index) {
        Select selectDepartingFromElem = new Select(driver.findElement(departingFromField));
        selectDepartingFromElem.selectByIndex(index);
    }

    public void selectArrivingIn(String city) {
        Select selectArrivingToElem = new Select(driver.findElement(arrivingInField));
        selectArrivingToElem.selectByValue(city);
    }
}

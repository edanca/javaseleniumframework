package pages;

import helpers.Helpers;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PageLogin {
    private WebDriver driver;

    @FindBy(how = How.NAME, using = "userName")
    private WebElement userFieldElement;
    //private By userField;

    @FindBy(how = How.NAME, using = "password")
    private WebElement passwordFieldElement;
    //private By passwordField;

    //@FindBy(tagName = "input")
    //private WebElement fieldsElements;
    private By fields;

    private By loginButtonField;
    private By topNavElems;


    public PageLogin(WebDriver driver) {
        this.driver = driver;
        //userField = By.name("userName");
        //passwordField = By.name("password");
        loginButtonField = By.name("login");
        fields = By.tagName("input");
        topNavElems = By.cssSelector("body > div > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > table > tbody > tr");
    }

    public void login(String user, String pass) {
        //WebElement userFieldElem = driver.findElement(userField);
        //WebElement passwordFieldElem = driver.findElement(passwordField);

        userFieldElement.sendKeys(user);
        //userFieldElem.sendKeys(user);
        passwordFieldElement.sendKeys(pass);
        passwordFieldElement.sendKeys(Keys.ENTER);
        //passwordFieldElem.sendKeys(pass);
        //passwordFieldElem.sendKeys(Keys.ENTER);

        File myScreenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        /*try {
            FileUtils.copyFile(myScreenshot, new File("LOGIN" + System.currentTimeMillis() + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        Helpers helper = new Helpers();
        helper.sleep(driver, 10);
    }

    public void loginTimeLoad(String user, String pass) {
        List<WebElement> loginFields = driver.findElements(fields);
        loginFields.get(2).sendKeys(user);
        loginFields.get(3).sendKeys(pass);
        //fieldsElements.get(2).sendKeys(user);
        //fieldsElements.get(3).sendKeys(pass);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void verifyFields() {
        //List<WebElement> loginFields = driver.findElements(fields);
        //System.out.println(loginFields.size());
        //Assert.assertTrue(loginFields.size()==5);
        //int fieldsSize = fieldsElements.size();
        //Assert.assertTrue(fieldsSize==5);
    }

    public void verifyTopNavbarElems() {
        List<WebElement> topNavbarFields = driver.findElements(topNavElems);

        String topFieldsExValues = "SIGN-ON REGISTER SUPPORT CONTACT";
        String topFieldsCurValues = "";
        topFieldsCurValues = topNavbarFields.get(0).getText();

        Assert.assertEquals(topFieldsCurValues, topFieldsExValues, "TOP FIELDS NOT MATCH");
    }

    public void validateTitle() {
        String title = driver.getTitle();
        Assert.assertEquals("Welcome: Mercury Tours", title);
    }
}
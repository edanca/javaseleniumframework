package testcases;

import helpers.Helpers;
import helpers.Screenshoter;
import helpers.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.PageLogin;
import pages.PageLogon;
import pages.PageReservation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Tests {

    private WebDriver driver;

    //GLOBAL VARIABLES-------------------------------------
    private String url = "http://newtours.demoaut.com/";
    private ArrayList<String> tabs;
    //----------------------------------------------

    @BeforeClass
    public void  setUpClass() {
        driver = new ChromeDriver();
        //driver.manage().window().maximize();
        //driver.manage().window().setPosition(new Point(200, 200));

        System.out.println("=========== TESTING STARTS ===========");
    }

    @AfterClass
    public void tearDownClass() {
        driver.close();
    }

    @BeforeMethod
    public void setUp() {
//        driver = new ChromeDriver();
//        driver.manage().window().maximize();
        driver.navigate().to(url);

        JavascriptExecutor jsExec = (JavascriptExecutor)driver;
        String googleWindow = "window.open('https://www.google.com')";
        jsExec.executeScript(googleWindow);
        tabs = new ArrayList<String>(driver.getWindowHandles());

        driver.switchTo().window(tabs.get(1)).navigate().to("https://youtube.com");
        driver.switchTo().window(tabs.get(0));

        Helpers helper = new Helpers();
        helper.sleep(driver, 1);

        System.out.println("=========== TEST CASE ===========");
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        driver.switchTo().window(tabs.get(1)).close();
        driver.switchTo().window(tabs.get(0));

        if(!result.isSuccess()) {
            Screenshoter.takeScreenshot("Error", driver);
        }
//        driver.close();
    }

    @Test
    public void testloginFailed() {
        String userValue = "user";
        String passValue = "user";

        WebDriverManager.setWindowsSize(driver, "maximize");
        PageLogin pageLogin = new PageLogin(driver);
        pageLogin.login(userValue, passValue);

        /* ============= VALIDATION ============= */
        PageLogon pageLogon = new PageLogon(driver);
        pageLogon.assertLogonPage();
    }

    @Test
    public void testloginSuccess() {
        String userValue = "mercury";
        String passValue = "mercury";

        PageLogin pageLogin = new PageLogin(driver);
        pageLogin.login(userValue, passValue);

        /* ============= VALIDATION ============= */
        PageReservation pageReservation = new PageReservation(driver);
        pageReservation.assertPage();
    }

    @Test
    public void testSelectFlight() {
        String userValue = "mercury";
        String passValue = "mercury";

        WebDriverManager.setWindowsSize(driver, "fullscreen");
        PageLogin pageLogin = new PageLogin(driver);
        pageLogin.login(userValue, passValue);

        PageReservation pageReservation = new PageReservation(driver);
        pageReservation.selectFlightPassengers(2);
        pageReservation.selectDepartingFrom(3);
        pageReservation.selectArrivingIn("New York");
    }

    @Test
    public void testTimeToLoadLoginPage() {
        String userValue = "mercury";
        String passValue = "mercury";

        WebDriverManager.setWindowsSizeByDimenssion(driver, 400, 500);
        PageLogin pageLogin = new PageLogin(driver);
        pageLogin.loginTimeLoad(userValue, passValue);
    }

    @Test
    public void testQuantityInputFields() {
        PageLogin pageLogin = new PageLogin(driver);
        pageLogin.verifyFields();
    }

    @Test
    public void testCountTopNavbar() {
        PageLogin pageLogin = new PageLogin(driver);
        pageLogin.verifyTopNavbarElems();
    }

    @Test
    public void testValidateTitle() {
        PageLogin pageLogin = new PageLogin(driver);
        pageLogin.validateTitle();
    }
}
